
public class PersonalInfoDemo {

	public static void main(String[] args) {
	
		Person me = new Person();
		Person myFriend01 = new Person();
		Person myFriend02 = new Person();
		
		me.SetName("Dmitry Vereykin");
		me.SetAge(22);
		me.SetAddress("333 Moon St.");
		me.SetPhone("(654)155-5527");
		
		myFriend01.SetName("Lisa Lee");
		myFriend01.SetAge(20);
		myFriend01.SetAddress("247 Sosong St.");
		myFriend01.SetPhone("(457)174-2435");
		
		myFriend02.SetName("Mary Smith");
		myFriend02.SetAge(24);
		myFriend02.SetAddress("432 Walnut St.");
		myFriend02.SetPhone("(742)852-3526");
		
		System.out.println("My information: ");
		System.out.println(" -Name: " + me.getName());
		System.out.println(" -Age: " + me.getAge());
		System.out.println(" -Address: " + me.getAddress());
		System.out.println(" -Phone: " + me.getPhone());
	      
		System.out.println("\nFriend #1's information. ");
		System.out.println(" -Name: " + myFriend01.getName());
		System.out.println(" -Age: " + myFriend01.getAge());
		System.out.println(" -Address: " + myFriend01.getAddress());
		System.out.println(" -Phone: " + myFriend01.getPhone());
	      
		System.out.println("\nFriend #2's information. ");
		System.out.println(" -Name: " + myFriend02.getName());
		System.out.println(" -Age: " + myFriend02.getAge());
		System.out.println(" -Address: " + myFriend02.getAddress());
		System.out.println(" -Phone: " + myFriend02.getPhone());
	}

}
