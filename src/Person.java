
public class Person {

	private String name, address, phone;
	private int age;
	
	public Person() {
		name = "";
		address = "";
		phone = "";
		age = 0;
	}
	
	public Person(String myName, String myAddress, int myAge, String myPhone) {
		name = myName;
		address = myAddress;
		phone = myPhone;
		age = myAge;
	}
	
	public void SetName(String myName) {
		name = myName;
	}
	
	public void SetAddress(String myAddress) {
		address = myAddress;
	}
	
	public void SetPhone(String myPhone) {
		phone = myPhone;
	}

	public void SetAge(int myAge) {
		age = myAge;
	}
	
	public String getName() {
		return name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public int getAge() {
		return age;
	}
}
